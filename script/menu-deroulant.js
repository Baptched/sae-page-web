/* Fonction faisant les modifications nécessaires pur l'affichage du menu déroulant */

function menu_deroulant() {

    const nav_menu = document.getElementById("nav-barre"); 
    const a_1 = document.getElementById("a-1");
    const a_2 = document.getElementById("a-2");
    const a_3 = document.getElementById("a-3");
    const a_4 = document.getElementById("a-4");
    const icon_hamburger = document.getElementById("icon-hamburger");
    const icon_croix = document.getElementById("icon-croix");
    const haut = document.getElementById("haut");
    /* Récupère les informations de différentes balise grâce à leur identifiant. On  stocke ces informmations dans différentes variables */


    if (icon_hamburger.classList.contains("active") || icon_hamburger.classList.contains("active1")){
    /* Condition qui permet de vérifier si icon_hamburger contient la class "active" ou "active1" soit que le menu déroulant n'est pas activé */
        
        /* Si le menu déroulant n'est pas ouvert il faut donc l'ouvrir */
        
        nav_menu.classList.add("active");
        /* Ajout au nav qui est dans le menu déroulant la class "active" */
        /* Cela permet d'ouvrir le menu déroulant */

        a_1.classList.add("active");
        a_2.classList.add("active");
        a_3.classList.add("active");
        a_4.classList.add("active");
        /* Ajout aux quatre <a> du menu déroulant la class "active" */
        /* Cela permet d'afficher les a dans le menu déroulant */
        
        if (icon_hamburger.classList.contains("active1")){
        /* Condition qui vérifie si l'icone hamburger possède la class "active1" */

            /* Si oui */
            icon_hamburger.classList.remove("active1");
            /* On lui enlève la class "active1" */
            /* Cela permet de déplacer l'icone hamburger car le menu est ouvert et de laisser la place à la croix de fermeture du menu */
        }
        
        else{
            /* Si non, cela signifie que l'icone hamburger à la classe "active" */
            icon_hamburger.classList.remove("active");
            /* On lui enlève la class "active" */
            /* Cela permet de déplacer l'icone hamburger car le menu est ouvert et de laisser la place à la croix de fermeture du menu */
        }
        
        icon_croix.classList.add("active");
        /* Ajout de la class "active" à l'icone croix */
        /* Cela déplace la croix de fermeture du menu à la place de l'icone hamburger */

        if (!haut.classList.contains("cache")){
        /* Condition qui vérifie si le bouton pour remonter en haut est visible */

            haut.classList.add("cache");
            /* Si il est visible, on lui ajoute la class "cache" */
            /* Cela permet de cacher le bouton */
        }
    }

    else {
    /* Si il ne contient pas la class "active" ou "active1" cela signifie que le menu est ouvert et il faut donc le fermer */

        a_1.classList.remove("active");
        a_2.classList.remove("active");
        a_3.classList.remove("active");
        a_4.classList.remove("active");
        /* Supprime aux quatre <a> du menu déroulant la class "active" */
        /* Cela permet d'enlever les a dans le menu déroulant */

        setTimeout( ()=> {
        /* Minuteur qui effectue la fonction fléchée anonyme après 0.5 secondes */
        /* Ce minuteur permet d'attendre que les animations de la supression des a du menu déroulant se finissent */
            
            nav_menu.classList.remove("active");
            /* Supression de la class "active" du nav du menu déroulant  */
            /* Cela enlève le nav du menu déroulant */

        },500);

        icon_croix.classList.remove("active");
        /* On enlève la class "active" à l'icone croix */
        /* Cela permet de déplacer l'icone croix car le menu est fermé et de laisser la place à l'icone hamburger du menu */

        icon_hamburger.classList.add("active");
        /* On ajoute la class "active" à l'icone hamburger */
        /* Cela déplace l'icone hamburger du menu à la place de la croix de fermeture */

        setTimeout( ()=> {
        /* Minuteur qui effectue la fonction fléchée anonyme après 0.7 secondes */
        /* Ce minuteur permet d'attendre que l'animation de la supression du nav du menu déroulant soit quasiment finie */

            if (haut.classList.contains("cache") && window.scrollY > 520){
            /* Condition qui vérifie si le bouton qui permet le retour en haut de la page doit être activé */

                haut.classList.remove("cache");
                /* On enlève la class "cache" du bouton */
                /* Cela permet d'afficher le bouton */

            }
        },700);
    }
}