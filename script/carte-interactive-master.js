var map = document.querySelector('#map') /* variable qui contient la map */
var paths = map.querySelectorAll('.map__image a') /* selectionne les différents paths (formes géométriques) */
var links = map.querySelectorAll('.map__list a') /* selectionne les différents liens */

let ecoles_polytech = ["06", "13", "34", "37", "38", "44", "45", "49", "54", "59", "63", "69", "74", "75", "91"] // la liste des départements ayant une école polytech
let ecoles_epitech = ["06", "13", "31", "33", "34", "35", "44", "54", "59", "67", "69", "75"]// la liste des départements ayant une école epitech
let ecoles_centralesupelec = ["35", "57", "91"]// la liste des départements ayant une école centrale supelec
let ecoles_epsi = ["31", "33", "34", "35", "38", "44", "51", "59", "62", "69", "75", "89"]// la liste des départements ayant une école epsi

var dictionnaire_nom_ecoles = new Map() // crée un dictionnaire pour récupérer les noms des villes où il y a une école dans les départements importants
dictionnaire_nom_ecoles.set("06", "Nice") // ajoute une clé ("06"), valeur ("Nice") au dictionnaire
dictionnaire_nom_ecoles.set("13", "Marseille")
dictionnaire_nom_ecoles.set("31", "Toulouse")
dictionnaire_nom_ecoles.set("33", "Bordeaux")
dictionnaire_nom_ecoles.set("34", "Montpellier")
dictionnaire_nom_ecoles.set("35", "Rennes")
dictionnaire_nom_ecoles.set("37", "Tours")
dictionnaire_nom_ecoles.set("38", "Grenoble")
dictionnaire_nom_ecoles.set("44", "Nantes")
dictionnaire_nom_ecoles.set("45", "Orléans")
dictionnaire_nom_ecoles.set("49", "Angers")
dictionnaire_nom_ecoles.set("51", "Reims")
dictionnaire_nom_ecoles.set("54", "Nancy")
dictionnaire_nom_ecoles.set("57", "Metz")
dictionnaire_nom_ecoles.set("59", "Lille")
dictionnaire_nom_ecoles.set("62", "Arras")
dictionnaire_nom_ecoles.set("63", "Clermont-Ferrand")
dictionnaire_nom_ecoles.set("67", "Strasbourg")
dictionnaire_nom_ecoles.set("69", "Lyon")
dictionnaire_nom_ecoles.set("74", "Annecy-Chambéry")
dictionnaire_nom_ecoles.set("75", "Paris")
dictionnaire_nom_ecoles.set("89", "Auxerre")
dictionnaire_nom_ecoles.set("91", "Paris Saclay")

var dictionnaire_nom_departements = new Map() // crée un dictionnaire pour récupérer les noms des départements à partir du numéro de département
dictionnaire_nom_departements.set("06", "Alpes-Maritimes")
dictionnaire_nom_departements.set("13", "Bouches-du-Rhône")
dictionnaire_nom_departements.set("31", "Haute-Garonne")
dictionnaire_nom_departements.set("33", "Gironde")
dictionnaire_nom_departements.set("34", "Hérault")
dictionnaire_nom_departements.set("35", "Ille-et-Vilaine")
dictionnaire_nom_departements.set("37", "Indre-et-Loire")
dictionnaire_nom_departements.set("38", "Isère")
dictionnaire_nom_departements.set("44", "Loire-Altantique")
dictionnaire_nom_departements.set("45", "Loiret")
dictionnaire_nom_departements.set("49", "Maine-et-Loire")
dictionnaire_nom_departements.set("51", "Marne")
dictionnaire_nom_departements.set("54", "Meurthe-et-Moselle")
dictionnaire_nom_departements.set("57", "Moselle")
dictionnaire_nom_departements.set("59", "Nord")
dictionnaire_nom_departements.set("62", "Pas-de-Calais")
dictionnaire_nom_departements.set("63", "Puy-de-Dôme")
dictionnaire_nom_departements.set("67", "Bas-Rhin")
dictionnaire_nom_departements.set("69", "Rhône")
dictionnaire_nom_departements.set("74", "Haute-Savoie")
dictionnaire_nom_departements.set("75", "Seine")
dictionnaire_nom_departements.set("89", "Yonne")
dictionnaire_nom_departements.set("91", "Essonne")

var dictionnaire_polytech = new Map() // crée un dictionnaire pour récupérer les liens des sites des écoles de polytech à partir du numéro de département avec l'école ("polytech_06")
dictionnaire_polytech.set("polytech_06", "https://polytech.univ-cotedazur.fr/")
dictionnaire_polytech.set("polytech_13", "https://polytech.univ-amu.fr/")
dictionnaire_polytech.set("polytech_34", "https://www.polytech.umontpellier.fr/")
dictionnaire_polytech.set("polytech_37", "https://polytech.univ-tours.fr/")
dictionnaire_polytech.set("polytech_38", "https://www.polytech-grenoble.fr/")
dictionnaire_polytech.set("polytech_44", "https://polytech.univ-nantes.fr/")
dictionnaire_polytech.set("polytech_45", "https://www.univ-orleans.fr/fr/polytech")
dictionnaire_polytech.set("polytech_49", "http://www.polytech-angers.fr/fr/index.html")
dictionnaire_polytech.set("polytech_54", "https://polytech-nancy.univ-lorraine.fr/")
dictionnaire_polytech.set("polytech_59", "https://www.polytech-lille.fr/")
dictionnaire_polytech.set("polytech_63", "https://www.uca.fr/collegium-technologie-sciences-pour-lingenieur/polytech-clermont-ferrand")
dictionnaire_polytech.set("polytech_69", "https://polytech.univ-lyon1.fr/")
dictionnaire_polytech.set("polytech_74", "https://www.polytech.univ-smb.fr/")
dictionnaire_polytech.set("polytech_75", "https://www.polytech.sorbonne-universite.fr/")
dictionnaire_polytech.set("polytech_91", "https://www.polytech.universite-paris-saclay.fr/")

var dictionnaire_epitech = new Map() // crée un dictionnaire pour récupérer les liens des sites des écoles de epitech à partir du numéro de département avec l'école
dictionnaire_epitech.set("epitech_06", "https://www.epitech.eu/fr/ecole-informatique-nice/")
dictionnaire_epitech.set("epitech_13", "https://www.epitech.eu/fr/ecole-informatique-marseille/")
dictionnaire_epitech.set("epitech_31", "https://www.epitech.eu/fr/ecole-informatique-toulouse/")
dictionnaire_epitech.set("epitech_33", "https://www.epitech.eu/fr/ecole-informatique-bordeaux/")
dictionnaire_epitech.set("epitech_34", "https://www.epitech.eu/fr/ecole-informatique-montpellier/")
dictionnaire_epitech.set("epitech_35", "https://www.epitech.eu/fr/ecole-informatique-rennes/")
dictionnaire_epitech.set("epitech_44", "https://www.epitech.eu/fr/ecole-informatique-nantes/")
dictionnaire_epitech.set("epitech_54", "https://www.epitech.eu/fr/ecole-informatique-nancy/")
dictionnaire_epitech.set("epitech_59", "https://www.epitech.eu/fr/ecole-informatique-lille/")
dictionnaire_epitech.set("epitech_67", "https://www.epitech.eu/fr/ecole-informatique-strasbourg/")
dictionnaire_epitech.set("epitech_69", "https://www.epitech.eu/fr/ecole-informatique-lyon/")
dictionnaire_epitech.set("epitech_75", "https://www.epitech.eu/fr/ecole-informatique-paris/")

var dictionnaire_centralesupelec = new Map() // crée un dictionnaire pour récupérer les liens des sites des écoles de centrale supelec à partir du numéro de département avec l'école
dictionnaire_centralesupelec.set("centralesupelec_35", "http://www.rennes.centralesupelec.fr/")
dictionnaire_centralesupelec.set("centralesupelec_57", "https://www.metz.centralesupelec.fr/")
dictionnaire_centralesupelec.set("centralesupelec_91", "https://www.centralesupelec.fr/fr/universite-paris-saclay")

var dictionnaire_epsi = new Map() // crée un dictionnaire pour récupérer les liens des sites des écoles de epsi à partir du numéro de département avec l'école
dictionnaire_epsi.set("epsi_31", "https://www.epsi.fr/campus/campus-de-toulouse/")
dictionnaire_epsi.set("epsi_33", "https://www.epsi.fr/campus/campus-de-bordeaux/")
dictionnaire_epsi.set("epsi_34", "https://www.epsi.fr/campus/campus-de-montpellier/")
dictionnaire_epsi.set("epsi_35", "https://www.epsi.fr/campus/campus-de-rennes/")
dictionnaire_epsi.set("epsi_38", "https://www.epsi.fr/campus/campus-de-grenoble/")
dictionnaire_epsi.set("epsi_44", "https://www.epsi.fr/campus/campus-de-nantes/")
dictionnaire_epsi.set("epsi_51", "https://www.epsi.fr/campus/campus-de-reims/")
dictionnaire_epsi.set("epsi_59", "https://www.epsi.fr/campus/campus-de-lille/")
dictionnaire_epsi.set("epsi_62", "https://www.epsi.fr/campus/campus-darras/")
dictionnaire_epsi.set("epsi_69", "https://www.epsi.fr/campus/campus-de-lyon/")
dictionnaire_epsi.set("epsi_75", "https://www.epsi.fr/campus/campus-de-paris/")
dictionnaire_epsi.set("epsi_89", "https://www.epsi.fr/campus/campus-de-auxerre/")

var resetInfos = function (){ // reset tous les liens (liens des départements à #), les targets (target à "") et les titles (redevienne le nom du département lorsqu'on survole celui-ci)
  map.querySelectorAll('.is-active').forEach(function (item){ // sélectionne tous les anciens départements sélectionnés (avec la classe active)
    item.classList.remove('is-active') /* enleve la classe active à un élément */
    item.setAttribute("xlink:href", "#") /* change l'attribut xlink:href à # soit qu'il ne redirige à aucun endroit */
    item.setAttribute("target", "") /* change l'attribut target à "" soit qu'il reste sur le même onglet */
    var numeroDepartement = item.id.replace('departement-', "") // récupère le numéro de département
    item.setAttribute("xlink:title", dictionnaire_nom_departements.get(numeroDepartement)) /* change l'attribut xlink:title par le nom du département "Alpes-Maritimes" au lieu de "Polytech Nice" par exemple */
  })
}

var activeArea = function (id, listes_departements, dictionnaire_ecole){ // met la classe active à tous les départements de la liste passée en paramètre
  listes_departements.forEach(function (numeroDepartement){ /* parcourt chaque departement */
    document.querySelector('#departement-'+ numeroDepartement).classList.add('is-active') /* met la classe active à cet élément (département) */
    var newLien = id + "_" + numeroDepartement // récupère la variable pour le nouveau lien
    document.querySelector('#departement-'+ numeroDepartement).setAttribute("xlink:href", dictionnaire_ecole.get(newLien)) // change l'attribut xlink:href par le nouveau lien de l'école dans ce département
    document.querySelector('#departement-'+ numeroDepartement).setAttribute("target", "_blank") // change l'attribut target à "_blank" pour faire en sorte que le département s'ouvre dans un nouvel onglet
    if (id != "centralesupelec"){ /* teste si l'école sélectionnée n'est pas centrale supelec (c'est juste un test pour un meilleur affichage quand on choisi centrale supelec comme école) */
      document.querySelector('#departement-'+ numeroDepartement).setAttribute("xlink:title", id + " " + dictionnaire_nom_ecoles.get(numeroDepartement)) // change l'attribut xlink:title par le nom de l'école avec la ville où elle se trouve
    }
    else{ // pour un affichage plus propre pour l'école -> exemple: Centrale Supelec Metz au lieu de centralesupelec Metz
      document.querySelector('#departement-'+ numeroDepartement).setAttribute("xlink:title", "Centrale Supelec" + " " + dictionnaire_nom_ecoles.get(numeroDepartement))
    }
  })
}

links.forEach(function (link){ /* parcourt chaque lien */
  link.addEventListener('click', function (){ /* attache une fonction à chaque fois que l'évenement (ici un clique sur un lien) est réussi */
    resetInfos() /* appelle la fonction resetInfos qui réinitialise tous les liens, targets et titles */
    var id = this.id.replace('list-', '') // récupère le nom de l'école
    document.querySelector('#list-'+ id).classList.add('is-active') /* donne la classe active au lien */
    if (id == "polytech"){ /* teste si le nom de l'école est polytech */
      activeArea(id, ecoles_polytech, dictionnaire_polytech) /* appelle la fonction activeArea avec les paramètres de polytech (ecoles_polytech et dictionnaire_polytech) qui sélectionne tous les départements de cette école */
    }
    else if (id == "epitech"){ /* teste si le nom de l'école est epitech */
      activeArea(id, ecoles_epitech, dictionnaire_epitech) /* appelle la fonction activeArea avec les paramètres de epitech (ecoles_epitech et dictionnaire_epitech) qui sélectionne tous les départements de cette école */
    }
    else if (id == "epsi"){ /* teste si le nom de l'école est epsi */
      activeArea(id, ecoles_epsi, dictionnaire_epsi) /* appelle la fonction activeArea avec les paramètres de epsi (ecoles_epsi et dictionnaire_epsi) qui sélectionne tous les départements de cette école */
    }
    else if (id == "centralesupelec"){ /* teste si le nom de l'école est centralesupelec */
      activeArea(id, ecoles_centralesupelec, dictionnaire_centralesupelec) /* appelle la fonction activeArea avec les paramètres de centralesupelec (ecoles_centralesupelec et dictionnaire_centralesupelec) qui sélectionne tous les départements de cette école */
    }
  })
})

