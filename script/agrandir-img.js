/* Script permettant d'agrandir les graphiques sur la page d'accueil et la page bachelor lorsque l'on clique sur le bouton dédié */

const bouton_agrandir = document.getElementById("bouton-agrandir");
const div_img_agrandir = document.getElementById("div-image-agrandir");
const bouton_fermeture = document.getElementById("bouton-fermeture");
const html = document.querySelector("html");
const header = document.querySelector("header");
/* Récupère les informations de différentes balises grâce à leur identifiant ou selon un selecteur. On stocke ces informmations dans différentes variables */


bouton_agrandir.addEventListener('click', () => {
/* Evènement qui se déclenche lorsque l'on clique sur le bouton pour agrandir le graphique */

    div_img_agrandir.style.display = "block";
    /* On modifie le display de la div à block */
    /* Cela permet de rendre la div visible */

    html.classList.add("active");
    /* Ajout au html de la class "active" */
    /* Cela permet d'enlever le scroll lorsque l'on est dans la "page" où l'image est agrandie */

    header.style.zIndex = "0";
    /* Modification du z-index du header à 0 */
    /* Cela permet de ne pas voir le header lorsque l'on est dans la "page" où l'image est agrandie */

});


bouton_fermeture.addEventListener('click', () => {
/* Evènement qui se déclenche lorsque l'on clique sur le bouton pour fermer la "page" où l'image est agrandie */

    div_img_agrandir.style.display = "none";
    /* On modifie le display de la div à none */
    /* Cela permet de rendre la div non visible */

    html.classList.remove("active");
    /* Suppression au html de la class "active" */
    /* Cela permet de remettre le scroll sur la page */

    header.style.zIndex = "1";
    /* Modification du z-index du header à 1 */
    /* Cela permet que les boutons qui sont en position relative ne passe pas au dessus du header lorsque l'on descend dans la page  */

});