/* Script pour le bouton retour en haut */

const haut = document.getElementById("haut"); /* Récupère les informations sur la balise avec comme identifiant "haut" et  stocke dans la variable haut */
const nav_menu = document.getElementById("nav-barre"); 


window.onscroll = () =>  { /* Evènement scroll se déclenche lorsque l'utilisateur fait défiler le contenu de la page */

    if (window.scrollY > 520 && !nav_menu.classList.contains("active")){ 
        /* Condition si le scroll vertical (donc celon l'axe y) est supérieur à 520 et que nous sommes pas dans le menu déroulant */
        
        /* Si il est supérieur */
        if (haut.classList.contains("cache")){ /* On vérifie si la balise avec comme identifiant "haut" possède la class "cache" */
            haut.classList.remove("cache"); /* Si elle posède la class, on lui enlève */
        }
    }

    else{

        /* Si il n'est pas supérieur */
        if (!haut.classList.contains("cache")){ /* On vérifie si la balise avec comme identifiant "haut" ne possède pas la class "cache" */
            haut.classList.add("cache"); /* Si elle ne posède pas la class, on lui ajoute */
        }
    }
}

/* Si la balise avec l'identifiant "haut" possède la class "cache" cela signifie que le bouton pour remonter en haut est caché */


haut.addEventListener('click', () => { /* Evènement qui se déclenche lorsque l'on clique sur le bouton pour remonter en haut de la page */
    window.scrollTo({ /* On remonte donc la page en haut avec l'animation "smooth" */
        top: 0,
        left: 0,
        behavior: "smooth"
    });
})