/* Script permetant l'affichage d'une demi fleche lors du survol du lien vers une autre page dans le menu déroulant */

const a_1 = document.getElementById("a-1"); 
/* Récupère les informations sur la balise avec comme identifiant "a-1" et  stocke dans la variable a_1 ce qui correspond au premier <a> du menu déroulant donc accueil */
const a_2 = document.getElementById("a-2");
/* Récupère les informations sur la balise avec comme identifiant "a-2" et  stocke dans la variable a_2 ce qui correspond au second <a> du menu déroulant donc Bachelor */
const a_3 = document.getElementById("a-3");
/* Récupère les informations sur la balise avec comme identifiant "a-3" et  stocke dans la variable a_3 ce qui correspond au troisième <a> du menu déroulant donc Master */
const a_4 = document.getElementById("a-4");
/* Récupère les informations sur la balise avec comme identifiant "a-4" et  stocke dans la variable a_4 ce qui correspond au quatrième <a> du menu déroulant donc École d'ingénieur */

a_1.addEventListener("mouseover", () => { /* Evènement qui se déclenche lorsque la souris survole le premier <a> et exécute la fonction anonyme fléchée qui suit */
    const demi_fleche = document.getElementById("demi-fleche-1"); 
    /* Récupère les informations sur la balise avec comme identifiant "demi-fleche-1" et  stocke dans la variable demi_fleche ce qui correspond à la demi flèche du premier <a> */
    demi_fleche.style.opacity = "1";
    /* Modifie l'opacité de cette balise à 1 */
})

a_1.addEventListener("mouseleave", () => { /* Evènement qui se déclenche lorsque la souris quitte le premier <a> et exécute la fonction anonyme fléchée qui suit */
    const demi_fleche = document.getElementById("demi-fleche-1");
    /* Récupère les informations sur la balise avec comme identifiant "demi-fleche-1" et  stocke dans la variable demi_fleche ce qui correspond à la demi flèche du premier <a> */
    demi_fleche.style.opacity = "0";
    /* Modifie l'opacité de cette balise à 0 */
})

a_2.addEventListener("mouseover", () => { /* Evènement qui se déclenche lorsque la souris survole le second <a> et exécute la fonction anonyme fléchée qui suit */
    const demi_fleche = document.getElementById("demi-fleche-2");
    /* Récupère les informations sur la balise avec comme identifiant "demi-fleche-2" et  stocke dans la variable demi_fleche ce qui correspond à la demi flèche du second <a> */
    demi_fleche.style.opacity = "1";    
    /* Modifie l'opacité de cette balise à 1 */
})

a_2.addEventListener("mouseleave", () => { /* Evènement qui se déclenche lorsque la souris quitte le second <a> et exécute la fonction anonyme fléchée qui suit */
    const demi_fleche = document.getElementById("demi-fleche-2"); 
    /* Récupère les informations sur la balise avec comme identifiant "demi-fleche-2" et  stocke dans la variable demi_fleche ce qui correspond à la demi flèche du second <a> */
    demi_fleche.style.opacity = "0";
    /* Modifie l'opacité de cette balise à 0 */
})

a_3.addEventListener("mouseover", () => { /* Evènement qui se déclenche lorsque la souris survole le troisième <a> et exécute la fonction anonyme fléchée qui suit */
    const demi_fleche = document.getElementById("demi-fleche-3");
    /* Récupère les informations sur la balise avec comme identifiant "demi-fleche-3" et  stocke dans la variable demi_fleche ce qui correspond à la demi flèche du troisième <a> */
    demi_fleche.style.opacity = "1";
    /* Modifie l'opacité de cette balise à 1 */
})

a_3.addEventListener("mouseleave", () => { /* Evènement qui se déclenche lorsque la souris quitte le troisième <a> et exécute la fonction anonyme fléchée qui suit */
    const demi_fleche = document.getElementById("demi-fleche-3"); 
    /* Récupère les informations sur la balise avec comme identifiant "demi-fleche-3" et  stocke dans la variable demi_fleche ce qui correspond à la demi flèche du troisième <a> */
    demi_fleche.style.opacity = "0";
    /* Modifie l'opacité de cette balise à 0 */
})

a_4.addEventListener("mouseover", () => {
    const demi_fleche = document.getElementById("demi-fleche-4"); /* Evènement qui se déclenche lorsque la souris survole le troisième <a> et exécute la fonction anonyme fléchée qui suit */
    /* Récupère les informations sur la balise avec comme identifiant "demi-fleche-4" et  stocke dans la variable demi_fleche ce qui correspond à la demi flèche du quatrième <a> */
    demi_fleche.style.opacity = "1";
    /* Modifie l'opacité de cette balise à 1 */
})

a_4.addEventListener("mouseleave", () => { /* Evènement qui se déclenche lorsque la souris quitte le quatrième <a> et exécute la fonction anonyme fléchée qui suit */
    const demi_fleche = document.getElementById("demi-fleche-4");
    /* Récupère les informations sur la balise avec comme identifiant "demi-fleche-4" et  stocke dans la variable demi_fleche ce qui correspond à la demi flèche du quatrième <a> */
    demi_fleche.style.opacity = "0";
    /* Modifie l'opacité de cette balise à 0 */
})

/* lorsque la balise avec l'identifiant "demi-fleche-1", "demi-fleche-2", "demi-fleche-3", "demi-fleche-4" à l'oppacité à 0 cela signifie 
que la demi flèche n'est pas visible et lorsque l'oppacité est à 1 alors elle est visible */