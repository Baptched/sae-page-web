# SAE 2.06 Recherche documentaire et production d’un site web

## Flash Info Métier

GitLab : https://gitlab.com/Baptched/sae-page-web   

Membres du groupe :   

CHÉDEVILLE Baptiste  
BALLUS Hugo  
BOURREAU Quentin  
DAHOUEDE Rebson  
DEVERS-DORÉ Lucas  


## Introduction

Dans le cadre de la SAE 2.06 "Recherche documentaire et production d’un site web", notre client, le département informatique de l’IUT d’Orléans, nous a demandé un site internet à destination des futurs bacheliers qui envisagent d’intégrer le BUT informatique. Nos informations doivent être utiles, fiables et perspicaces pour donner envie aux jeunes diplômés de nous rejoindre pour les futures années.   

Pour la création de ce site, nous avons réalisés deux étapes : tout d'abord une recherche documentaire sur un des trois sujets, puis la conception d'un site respectant les normes CSS3 et HTML5, d'un minimumde  trois pages.  


## Ce que nous avons fait

Pour ce projet, nous avons effectué une recherche documentaire sur les métiers de l’informatique.  
Pour présenter ce sujet, nous avons décidé de le scinder en trois parties en suivant les trois possibilités d'études possible suite à un BUT informatique. Soit le bachelor (bac +3) avec seulent le BUT informatique, l'école d'ingénieur (bac +5) et enfin le master (bac +5).  
Pour chaque choix, nous avons présenté des métiers accessibles avec ce niveau d'étude, d'une manière complète avec des informations précises et des chiffres ainsi que des graphiques.  

Nous avons donc conçu 4 pages. 3 pages sur les différents niveaux d'études et une page d'accueil introductif au site et aux métiers de l'informatique.

Pour accrocher le lecteur, nous avons également utilisés les différentes fonctions du langages de Jackobson.   

En terme de code, nous avons cherché à proposer un code maintenable, c'est-à-dire que nous avons considéré que notre client pouvait nous demander, à nous ou à d'autres développeurs, des amméliorations et modifications dans les prochaines années.  
Pour se préparer a cela, nous avons commenté et organisé tous nos codes (html, css et javascript) afin de le rendre clairs et faccielement compréhensible. Ainsi, l'ajout et les modifications se font sans difficultés.

Nous avons d'abord fabriqué deux maquettes pour chaque page, une en grand format pour un écran de taille standard, et une seconde pour les écrans plus petit tel qu'un smartphone ou une tablette. Maquette retrouvable sur le GitLab : https://gitlab.com/Baptched/sae-page-web/-/tree/main/maquettes, ces maquettes étaient dans le dossier `maquettes` mais prennaient trop de place pour respecter les 50 Mo de limite.   
Nous avons aussi élaboré une maquette pour le menu déroulant lorsque l'écran est inférieur à 1200 pixels en largeur.  

Après la création des différentes maquettes, et avant de s'attaquer au code, nous avons créé deux DOM de la structure html de la page d'accueil et des trois autres pages qui on quasiement la même structure. DOM retrouvable en pdf dans le dossier `DOM`.   
Les pages sont composées d'un header et d'un footer commun, et d'un texte d'introduction.  
Les pages sur les trois possibilités de poursuites d'étude sont composées dans le main d'une liste de section/arcticles/div qui présente les différents métiers.
Ces trois pages ont un aside différent avec pour la page bachelor un graphique avec possibilité de zoom ainsi qu'une liste d'IUT proche de la région.   
Pour la page master, une carte intéractive présente les différents master disponibles en France et dans chaque département, en choisissant dans un premier temps une école puis un département.   
La page école d'ingénieur a un aside composé d'une quinzaine d'école d'ingénieur en informatique.  

Après ces deux DOM créés, nous avons commencé à coder les 4 pages, tout en commentant le code au fur et à mesure de son écriture, nous avons aussi vérifié que le html et le css passent bien le validateur et respecté les bonnes méthodes et pratiques des différents langages.  

Nous avons aussi fait attention que le site soit facilement lisible en choisisant une police sans sérif adapté au web, ainsi qu'il soit résponsive pour correspondre à la totalité des appareils que cela soit sur grand écran, smartphone ou tablette.  


## Html

Notre site est composé de quatre pages, donc quatre page html aussi.   
La page d'accueil qui à pour nom `index.html`, la page master `master.html`, bachelor `bachelor.html` et enfin la page école d'ingénieur dans le fichier ecole-ingenieur.html.       


## CSS

Le css est réparti de différentes façons, il y a un css commun au quatre pages dans le dossier `css` `general_positionnement.css`, dans cette feuille de style, il y a le css du header, le footer ainsi que toute les déclarations qui sont communes à chaque page.    

Une deuxième feuille de style est commune aux quatres pages, il s'agit de la feuille `menu-deroulant.css` qui gère seuleuniquement le menu déroulant qui apparait lorsque l'écran est inférieur à 1200 pixels.   

Nous avons fait le choix de mettre des feuilles de style communes, afin d'effectuer une seule modification qui se répercute sur les 4 pages en cas de modification sur le header, le footer ou le menu déroulant, et de ne pas avoir besoin de modifier plusieurs feuilles de style.   

Ensuite chaque page dispose d'une feuille de style qui lui est attribuée pour coder cette même page.
La page pour l'accueil est `index_positionnement.css`, `bachelor_positionnement`pour la page sur le bachelor, pour la page sur l'école d'ingénieur : `ingenieur_positionnement.css` et enfin `master_positionnement.css` pour celle sur le master.  

La page master dispose d'une seconde feuille de style lui étant réservée, c'est la feuille `carte-interactive.css`, elle permet le positionnement de la carte intéractive présente dans le aside de la page.  

Chaque feuille de style possède un sommaire pour pouvoir s'y retrouver facilement.  
Exemple sur la page de positionnement général : 

<img src="Img/sommaire-general-positionnment.png" width="350">


## Javascript 

Pour le Javascript, un langage que nous n'avions jamais vu en cours, il a fallut se débrouiller et se documenter par nous même sur ce nouveau langage pour pouvoir le comprendre, l'utiliser correctement et enfin l'implémenter sur notre site.  

Le Javascript a permis d'amméliorer notre site, avec par exemplela carte intéractive de la page sur le master ou encore le menu déroulant et bien d'autres améliorations.  

Le nombre de page de script retrouvable dans le dossier `script` est au nombre de cinq.   
Une page pour la carte intéractive `carte-interactive-master.js`, une autre pour le menu déroulant `menu-deroulant.js` une autre toujours dans le menu déroulant pour le hover des liens `hover-demi-fleche.js`.   
Une autre page `agrandir-img.js` pour agrandir les deux graphiques des pages bachelor et d'accueil, afin de mieux voir les détails des-dits graphiques et la dernière page `bouton-retour-haut.js` qui permet d'afficher un bouton pour remonter en haut de page lorsque le scroll vertical est supérieur à 520.  


## Autre 

Il y a deux autres répertoires, le premier qui est `img` qui contient les images que nous avons utilisés pour le site, tous les logos, images des différentes parties, graphiques.   
Il y a aussi les images pour le README.    

Le second dossier est le dossier `police` qui contient 3 fichers `woff2` pour inclure la police `Font Awesome 5 Pro` une police déchiffrant l'unicode pour notre bouton hamburger et fermeture du menu déroulant.  


## Bilan

Pour ce projet, nous souhaitions surtout découvrir de nouvelles choses que nous n'avions pas vu en cours, comme des langages très complets.  
Ainsi,  nous avons appris le nécessaire en Javascript pour faire ce que l'on voulait (carte intéractive, menu ...).    
Pour apprendre, nous avons décidé d'utiliser différente méthodes pour les images avec les images svg pour la carte intéractive mais aussi les demi-flèches du menu déroulant.   
Le "dessin" en pur css en utilisant les pseudos-éléments `before` et `after` en jouant sur leur contenu, les bordures et la couleur de l'arrière plan pour dessiner une flèche dans le bouton retour en haut, ou pour les boutons d'ouverture et de fermeture du menu déroulant.    

Pour ce qui est de la résponsivité du site, c'est la première fois que nous apréhendions cela, nous nous sommes renseigné sur le sujet comme pour le Javascrip, nous avons vu les différentes méthodes existantes e choisi l'une d'elle' pour l'appliquer sur ce site.   

Ce projet a été riche en apprentissages, et nous a entraîneé à travailler en équipe, à nous organiser, à nous répartir le travail, et faire des choix, comme nous le ferons dans l'avenir lors de la pratique de notre métier d'informaticien. 